<?php 

use Illuminate\Database\Seeder;

class PsbSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('psbs')->insert(['thnpelajaran_id' => 2, 'name' => 'PSB Thn. 2017', 'description' => '9990124882929', 'start_at' => '2016/8/25', 'end_at' => '2017/8/25']);
    }
}