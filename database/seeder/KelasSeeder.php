<?php 

use Illuminate\Database\Seeder;

class KelasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelases')->insert(['thnpelajaran_id' => 1,'guru_id' => 1, 'kelas' => 'A', 'tingkat' => 7, 'kapasitas' => 30]);
        DB::table('kelases')->insert(['thnpelajaran_id' => 1,'guru_id' => 2, 'kelas' => 'B', 'tingkat' => 8, 'kapasitas' => 28]);
        DB::table('kelases')->insert(['thnpelajaran_id' => 1,'guru_id' => 3, 'kelas' => 'C', 'tingkat' => 9, 'kapasitas' => 29]);
        DB::table('kelases')->insert(['thnpelajaran_id' => 2,'guru_id' => 2, 'kelas' => 'A', 'tingkat' => 7, 'kapasitas' => 29]);
        DB::table('kelases')->insert(['thnpelajaran_id' => 2,'guru_id' => 3, 'kelas' => 'B', 'tingkat' => 8, 'kapasitas' => 30]);
        DB::table('kelases')->insert(['thnpelajaran_id' => 2,'guru_id' => 1, 'kelas' => 'C', 'tingkat' => 9, 'kapasitas' => 28]);
    }
}