<?php 

use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('siswas')->insert(['nisn' => '1248937592387', 'kelas_id' => 1, 'name' => 'Agus Permana']);
        DB::table('siswas')->insert(['nisn' => '2434862385645', 'kelas_id' => 2, 'name' => 'Fitri Angraini']);
    }
}