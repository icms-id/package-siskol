<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(TahunSeeder::class);
		$this->call(PsbSeeder::class);
		$this->call(JamSeeder::class);
		$this->call(PelajaranSeeder::class);
		$this->call(GuruSeeder::class);
		$this->call(KelasSeeder::class);
		$this->call(SiswaSeeder::class);
	}
}
