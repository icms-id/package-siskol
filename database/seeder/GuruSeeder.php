<?php 

use Illuminate\Database\Seeder;

class GuruSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gurus')->insert(['pelajaran_id' => 1, 'name' => 'Agus Triatmojo S.Pd', 'nip' => '9990124882929']);
        DB::table('gurus')->insert(['pelajaran_id' => 2, 'name' => 'Nunung Sartika M.Pd', 'nip' => '9124945385656']);
        DB::table('gurus')->insert(['pelajaran_id' => 3, 'name' => 'Ayu Wulandari S.Pd',  'nip' => '9812386645473']);
    }
}