<?php 

use Illuminate\Database\Seeder;

class TahunSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('thn_pelajarans')->insert(['thn_start' => 2016, 'thn_end' => 2017, 'is_active' => true]);
        DB::table('thn_pelajarans')->insert(['thn_start' => 2017, 'thn_end' => 2018, 'is_active' => false]);
    }
}