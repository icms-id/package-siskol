<?php 

use Illuminate\Database\Seeder;

class JamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '07:10', 'end_at' => '07:50']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '07:10', 'end_at' => '07:50']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '07:10', 'end_at' => '07:50']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '07:10', 'end_at' => '07:50']);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '07:10', 'end_at' => '07:50']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '07:10', 'end_at' => '07:50']);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '07:50', 'end_at' => '08:30']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '07:50', 'end_at' => '08:30']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '07:50', 'end_at' => '08:30']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '07:50', 'end_at' => '08:30']);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '07:50', 'end_at' => '08:30']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '07:50', 'end_at' => '08:30']);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '08:30', 'end_at' => '09:10']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '08:30', 'end_at' => '09:10']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '08:30', 'end_at' => '09:10']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '08:30', 'end_at' => '09:10']);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '08:30', 'end_at' => '09:10']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '08:30', 'end_at' => '09:10']);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '09:10', 'end_at' => '09:50']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '09:10', 'end_at' => '09:50']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '09:10', 'end_at' => '09:50']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '09:10', 'end_at' => '09:50']);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '09:10', 'end_at' => '09:50']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '09:10', 'end_at' => '09:50']);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '09:50', 'end_at' => '10:05', 'is_break' => true]);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '09:50', 'end_at' => '10:05', 'is_break' => true]);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '09:50', 'end_at' => '10:05', 'is_break' => true]);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '09:50', 'end_at' => '10:05', 'is_break' => true]);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '09:50', 'end_at' => '10:05', 'is_break' => true]);
    	DB::table('jams')->insert(['day_num' => 6, 'start_at' => '09:50', 'end_at' => '10:05', 'is_break' => true]);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '10:05', 'end_at' => '10:45']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '10:05', 'end_at' => '10:45']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '10:05', 'end_at' => '10:45']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '10:05', 'end_at' => '10:45']);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '10:05', 'end_at' => '10:45']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '10:05', 'end_at' => '10:45']);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '10:45', 'end_at' => '11:25']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '10:45', 'end_at' => '11:25']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '10:45', 'end_at' => '11:25']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '10:45', 'end_at' => '11:25']);
        DB::table('jams')->insert(['day_num' => 5, 'start_at' => '10:45', 'end_at' => '11:25']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '10:45', 'end_at' => '11:25']);

        DB::table('jams')->insert(['day_num' => 1, 'start_at' => '11:25', 'end_at' => '12:05']);
        DB::table('jams')->insert(['day_num' => 2, 'start_at' => '11:25', 'end_at' => '12:05']);
        DB::table('jams')->insert(['day_num' => 3, 'start_at' => '11:25', 'end_at' => '12:05']);
        DB::table('jams')->insert(['day_num' => 4, 'start_at' => '11:25', 'end_at' => '12:05']);
        DB::table('jams')->insert(['day_num' => 6, 'start_at' => '11:25', 'end_at' => '12:05']);
    }
}