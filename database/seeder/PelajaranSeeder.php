<?php 

use Illuminate\Database\Seeder;

class PelajaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pelajarans')->insert(['kode_mat' => 'MAT', 'pelajaran' => 'Matematika']);
        DB::table('pelajarans')->insert(['kode_mat' => 'IPS', 'pelajaran' => 'Ilmu Pengetahuan Sosial']);
        DB::table('pelajarans')->insert(['kode_mat' => 'IPA', 'pelajaran' => 'Ilmu Pengetahuan Alam']);
    }
}