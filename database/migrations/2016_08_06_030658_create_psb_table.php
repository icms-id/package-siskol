<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePsbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thn_pelajarans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('thn_start')->unsigned();
            $table->integer('thn_end')->unsigned();
            $table->boolean('is_active')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('psbs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('thnpelajaran_id')->unsigned();
            $table->string('name', 40);
            $table->string('description')->nullable();
            $table->boolean('is_active')->default(false);
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->timestamps();
        });

        Schema::create('psb_calons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('psb_id')->unsigned();
            $table->json('data')->nullable();
            $table->json('nilai')->nullable();
            $table->string('login_id');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('psb_calons');
        Schema::dropIfExists('psbs');
        Schema::dropIfExists('thn_pelajarans');
    }
}
