<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jams', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('day_num')->unsigned();
            $table->time('start_at');
            $table->time('end_at');
            $table->string('keterangan')->nullable();
            $table->boolean('is_break')->default(false);
        });

        Schema::create('jadwals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelas_id')->unsigned();
            $table->integer('jam_id')->unsigned();
            $table->integer('pelajaran_id')->unsigned();

            $table->foreign('jam_id')->references('id')->on('jams')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
        Schema::dropIfExists('jams');
    }
}
