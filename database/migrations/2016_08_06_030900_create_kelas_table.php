<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('thnpelajaran_id')->unsigned();
            $table->integer('guru_id')->unsigned();
            $table->string('kelas', 20);
            $table->tinyInteger('tingkat')->unsigned();
            $table->tinyInteger('kapasitas')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelases');
    }
}
