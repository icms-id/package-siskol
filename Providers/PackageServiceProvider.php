<?php 

namespace Package\Nothing628\Siskol\Providers;

use ICMS\Package\PackageServiceProvider as ServiceProvider;

class PackageServiceProvider extends ServiceProvider {
	protected $providesRoute = [
		'PSB Online' => 'siskol.psb.online',
		'PSB Login' => 'siskol.psb.login',
	];

	public function boot()
	{
		//
	}

	public function register()
	{
		parent::register();
	}
}