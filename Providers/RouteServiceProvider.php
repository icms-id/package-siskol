<?php 

namespace Package\Nothing628\Siskol\Providers;

use ICMS\Package\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {
	protected $namespace = 'Package\Nothing628\Siskol\Http\Controllers';
	protected $slug = 'siskol';
}