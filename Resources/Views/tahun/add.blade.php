@extends('base')

@section('title')
Tambah Tahun Pelajaran
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post">
	{!! csrf_field() !!}

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12 form-group">
			<input type="text" class="form-control" data-inputmask="'mask': '2099'" name="thn" required="" id="prevYear">
		</div>
		<label class="control-label col-md-1 col-sm-1 col-xs-12" style="text-align: center;">-</label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<input class="form-control" type="text" readonly="" id="nextYear">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Active</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="checkbox" name="is_active" class="js-switch" /> 
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('siskol.tahun.index') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>

<link rel="stylesheet" type="text/css" href="{{ asset('css/switchery.min.css') }}">
<script type="text/javascript" src="{{ asset('js/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/jquery.inputmask.bundle.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var syncTahun = function (evt) {
			var val = $('#prevYear').val();

			if (isNaN(val)) {
				setValid(evt, false);
				$('#nextYear').val('');
			} else {
				setValid(evt, true);
				$('#nextYear').val(parseInt(val) + 1);
			}
		};

		var setValid = function (evt, is_valid) {
			if (typeof evt != 'undefined') {
				if (! is_valid) {
					evt.target.setCustomValidity("Silahkan masukan tahun pelajaran.");
				} else {
					evt.target.setCustomValidity("");
				}
			}
		};

		$(":input").inputmask();
		$('#prevYear').keyup(syncTahun);
	});
</script>
@endsection
