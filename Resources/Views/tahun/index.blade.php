@extends('base')

@section('title')
Tahun Pelajaran
@endsection

@section('package.content')
<a href="{{ Package::route('siskol.tahun.add') }}" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a>
<table class="table table-bordered" id="data">
	<thead>
		<tr>
			<th>Tahun Pelajaran</th>
			<th>Aktif</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/dataTables.bootstrap.min.css') }}">

<script type="text/javascript" src="{{ Package::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/responsive.bootstrap.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#data').DataTable({
			serverSide: true,
			ajax: {
				url: "{{ Package::route('siskol.tahun.ajax') }}",
				type: "POST"
			},
			columns: [
				{data: "Tahun"},
				{
					data: "Active",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = '';
						if (oData.Active) {
							content = "<i class=\"fa fa-check green\"></i>";
						} else {
							content = "<a href=\"{{ Package::route('siskol.tahun.active') }}/"+oData.ID+"\"><i class=\"fa fa-times red\"></i></a>";
						}

						$(nTd).html(content);
					}
				},
				{
					data: "ID",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<a href=\"{{ Package::route('siskol.tahun.edit') }}/"+oData.ID+"\">Edit</a> ";
						content += "<a href=\"{{ Package::route('siskol.tahun.delete') }}/"+oData.ID+"\">Delete</a> ";
						$(nTd).html(content);
					}
				}
			],
			responsive: true,
			ordering: false
		});
	});
</script>
@endsection
