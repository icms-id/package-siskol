@extends('base')

@section('title')
Tambah PSB Online
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun Pelajaran <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<select class="form-control" id="tahun-select" name="thnpelajaran_id">
				@foreach($tahun as $value)
				<option value="{{ $value->id }}">{{ $value->fullyear }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Judul PSB <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" name="name" maxlength="50" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea name="description" class="form-control" rows="2"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Waktu PSB <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="input-prepend input-group">
				<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
				<input type="text" style="width: 200px" id="start-picker" class="form-control" />
				<input type="hidden" name="start_at" id="start-at">
				<input type="hidden" name="end_at" id="end-at">
			</div>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Aktif</label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input type="checkbox" name="is_active" class="js-switch">
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('siskol.psb.index') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>

<link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/switchery.min.css') }}">
<script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/switchery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
<script src="{{ Package::asset('js/daterangepicker.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var optionSet1 = {
			startDate: moment(),
			endDate: moment().add(3, 'days'),
			minDate: moment(),
			maxDate: moment().add(1, 'years'),
			showDropdowns: true,
			showWeekNumbers: false,
			timePicker: false,
			opens: 'right',
			format: 'MM/DD/YYYY',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Clear',
				fromLabel: 'From',
				toLabel: 'To',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		};

		$('#start-picker').daterangepicker(optionSet1, function(start, end, label) {
			//
		}).on('apply.daterangepicker', function (evt, daterange) {
			$('#start-at').val(daterange.startDate.format('YYYY-MM-DD'));
			$('#end-at').val(daterange.endDate.format('YYYY-MM-DD'));
		});

		$('#tahun-select').select2({allowClear:false, placeholder: "Pilih tahun ajaran"});
	});
</script>
@endsection
