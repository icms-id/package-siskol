<div class="heading"></div>

<div class="row">
	<div class="col-md-6 col-md-offset-3 col-xs-12">
		<div class="box-simple box-white">
			<h3>Berhasil Mendaftar PSB Online</h3>
			<p>Selamat, anda telah berhasil melakukan pendaftaran secara online. Informasi penerimaan siswa akan dikirimkan melalui E-Mail/SMS. Selanjutnya, anda dapat mencetak formulir ini untuk dilakukan pendaftaran ulang jika diperlukan.</p>

			<div class="form-horizontal">
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">No Pendaftaran</label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<p class="form-control-static text-left">001/001/4584</p>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Daftar</label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<p class="form-control-static text-left">24 Mar 2016</p>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Login ID</label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<p class="form-control-static text-left">{{ $calon->login_id }}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
					<div class="col-md-3 col-sm-3 col-xs-12">
						<p class="form-control-static text-left">{{ $calon->password }}</p>
					</div>
				</div>
			</div>

			<p>Anda dapat melakukan pengubahan data sebelum psb ditutup menggunakan login ID dan password yang tertera di atas.</p>

			<div class="btn-group">
				<button class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<button class="btn btn-success"><i class="fa fa-print"></i> Cetak</button>
			</div>
		</div>
	</div>
</div>
