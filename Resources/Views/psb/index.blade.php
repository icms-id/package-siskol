@extends('base')

@section('title')
PSB Online
@endsection

@section('package.content')
<a href="{{ Package::route('siskol.psb.add') }}" class="btn btn-info"><i class="fa fa-pencil"></i> Add</a>
<table class="table table-bordered" id="data">
	<thead>
		<tr>
			<th>Nama</th>
			<th>Thn Ajaran</th>
			<th>Waktu Pendaftaran</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/dataTables.bootstrap.min.css') }}">

<script type="text/javascript" src="{{ Package::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/responsive.bootstrap.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#data').DataTable({
			serverSide: true,
			ajax: {
				url: "{{ Package::route('siskol.psb.ajax') }}",
				type: "POST"
			},
			columns: [
				{data: "Nama"},
				{data: "Tahun"},
				{data: "Waktu"},
				{
					data: "Status",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = '';
						if (oData.Status) {
							content = "<i class=\"fa fa-check green\"></i>";
						} else {
							content = "<i class=\"fa fa-times red\"></i>";
						}

						$(nTd).html(content);
					}
				},
				{
					data: "ID",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<a href=\"{{ Package::route('siskol.psb.edit') }}/"+oData.ID+"\">Edit</a>";
						content += " <a href=\"{{ Package::route('siskol.psb.delete') }}/"+oData.ID+"\">Delete</a>";
						$(nTd).html(content);
					}
				}
			],
			responsive: true,
			ordering: false
		});
	});
</script>
@endsection
