<div class="heading">
	<h3>Form Pendaftaran</h3>
</div>

<form class="form-horizontal form-label-left" method="post" enctype="form-data/multipart">
{!! csrf_field() !!}
<div class="tabs" style="margin-bottom: 20px;">
	<ul class="nav nav-pills nav-justified">
		<li class="active"><a href="#tab2-1" data-toggle="tab">Data Siswa</a></li>
		<li><a href="#tab2-2" data-toggle="tab">Data Ortu/Wali</a></li>
		<li><a href="#tab2-3" data-toggle="tab">Data Nilai</a></li>
	</ul>
	<div class="tab-content tab-content-inverse">
		<div class="tab-pane active" id="tab2-1">
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="nama" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Kelamin <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input data-parsley-id="12" data-parsley-multiple="gender" checked name="jenis_kelamin" value="L" type="radio"> Laki Laki
						</label>
						<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input data-parsley-multiple="gender" name="jenis_kelamin" value="P" type="radio"> Perempuan
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Tempat Lahir <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="tmp_lahir" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal Lahir <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="input-prepend input-group">
						<span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
						<input type="text" style="width: 200px" name="tgl_lahir" id="tgl_lahir" class="form-control" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="2" name="alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">No.Telp</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="no_telp" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Sekolah Asal <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="sekolah_nama" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Sekolah Asal</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="2" name="sekolah_alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Saudara Kandung</label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input type="text" name="s_kandung" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Saudara Tiri</label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input type="text" name="s_tiri" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Saudara Angkat</label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input type="text" name="s_angkat" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Riwayat Penyakit</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="riwayat_penyakit" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Tinggi Badan</label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input type="text" name="tinggi" class="form-control">
				</div>
				<label class="control-label col-md-2 col-sm-2 col-xs-12">Berat Badan</label>
				<div class="col-md-2 col-sm-2 col-xs-12">
					<input type="text" name="berat" class="form-control">
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab2-2">
			<div class="form-group">
				<h4 class="control-label col-md-3 col-sm-3 col-xs-12">Data Ayah</h4>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input data-parsley-id="12" data-parsley-multiple="gender" checked name="ayah_status" value="ada" type="radio"> Ada
						</label>
						<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input data-parsley-multiple="gender" name="ayah_status" value="wafat" type="radio"> Wafat
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="ayah_nama" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="2" name="ayah_alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select class="form-control" name="ayah_pendidikan">
						<option></option>
						<option value="sd">SD</option>
						<option value="smp">SMP</option>
						<option value="sma">SMA</option>
						<option value="sarjana">Sarjana</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Agama <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select class="form-control" name="ayah_agama">
						<option></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan <span class="required">*</span></label>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<input type="text" name="ayah_pekerjaan" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">No.Telp/HP <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="ayah_telp" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendapatan</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="ayah_pendapatan" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<h4 class="control-label col-md-3 col-sm-3 col-xs-12">Data Ibu</h4>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div id="gender" class="btn-group" data-toggle="buttons">
						<label class="btn btn-default active" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input data-parsley-id="12" data-parsley-multiple="gender" checked name="ibu_status" value="ada" type="radio"> Ada
						</label>
						<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
							<input data-parsley-multiple="gender" name="ibu_status" value="wafat" type="radio"> Wafat
						</label>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap <span class="required">*</span></label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="ibu_nama" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="2" name="ibu_alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendidikan <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select class="form-control" name="ibu_pendidikan">
						<option></option>
						<option value="sd">SD</option>
						<option value="smp">SMP</option>
						<option value="sma">SMA</option>
						<option value="sarjana">Sarjana</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Agama <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<select class="form-control" name="ibu_agama">
						<option></option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pekerjaan <span class="required">*</span></label>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<input type="text" name="ibu_pekerjaan" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">No.Telp/HP <span class="required">*</span></label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="ibu_telp" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Pendapatan</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="ibu_pendapatan" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<h4 class="control-label col-md-3 col-sm-3 col-xs-12">Data Wali</h4>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<input type="text" name="wali_nama" class="form-control">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<textarea class="form-control" rows="2" name="wali_alamat"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">No.Telp/HP</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" name="wali_telp" class="form-control">
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab2-3">
			<h4>Nilai Ujian</h4>
			<br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Mata Pelajaran</th>
						<th>Nilai Ujian</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($pelajaran as $pel)
					<tr>
						<td>{{ $pel }}</td>
						<td><input type="number" name="nilai_{{ $pel }}"></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
		<div class="btn-group">
			<a href="" class="btn btn-danger">Batal</a>
			<button class="btn btn-success">Submit Data</button>
		</div>
	</div>
</div>
</form>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/daterangepicker.css') }}">

<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ Package::asset('js/daterangepicker.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var optionSet1 = {
			maxDate: moment().add(1, 'years'),
			showDropdowns: true,
			singleDatePicker: true,
			calender_style: "picker_3"
		};

		$('#tgl_lahir').daterangepicker(optionSet1, function(start, end, label) {});
	});
</script>
