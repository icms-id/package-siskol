<div class="heading"></div>

<div class="row">
	<div class="col-md-4 col-md-offset-4 col-xs-12">
		<div class="box-simple box-white">
			<h3>Ubah data PSB</h3>
			<p>Silahkan masukkan login ID dan password.</p>

			<form method="post">
				{!! csrf_field() !!}
				<div class="form-group">
					<input type="text" name="login_id" class="form-control" placeholder="Login ID">
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Password">
				</div>
				<div class="form-group">
					<button class="btn btn-success" type="submit">Log In</button>
				</div>
			</form>

		</div>
	</div>
</div>
