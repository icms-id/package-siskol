<div class="heading">
	<h3>Selamat datang di PSB Online</h3>
</div>

<p>Berikut adalah jadwal pendaftaran siswa baru.</p>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>PSB</th>
					<th>Jadwal Pendaftaran</th>
					<th class="text-center">Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach($psb as $value)
				<tr>
					<td>{{ $value->name }}</td>
					<td>{{ $value->jadwal_str }}</td>
					<td class="text-center">
						@if ($value->status == 0)
						Belum dibuka
						@else
						<a href="{{ resolveRoute('siskol.psb.daftar', ['id' => $value->id]) }}" class="btn btn-sm btn-success">Daftar</a>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<p>Persyaratan PSB Online :</p>
