@extends('base')

@section('title')
Jam Belajar
@endsection

@section('package.content')
<div id='calendar'></div>
<div id="fc_create" data-toggle="modal" data-target="#CalenderModalNew"></div>
<div id="fc_edit" data-toggle="modal" data-target="#CalenderModalEdit"></div>

<div id="CalenderModalNew" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">New Calendar Entry</h4>
			</div>
			<div class="modal-body">
				<div id="testmodal" style="padding: 5px 20px;">
					<form id="antoform" class="form-horizontal calender" role="form">
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="title" name="title">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-9">
								<textarea class="form-control" style="height:55px;" id="descr" name="descr"></textarea>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default antoclose" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary antosubmit">Save changes</button>
			</div>
		</div>
	</div>
</div>
<div id="CalenderModalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel2">Edit Calendar Entry</h4>
			</div>
			<div class="modal-body">
				<div id="testmodal2" style="padding: 5px 20px;">
					<form id="antoform2" class="form-horizontal calender" role="form">
						<div class="form-group">
							<label class="col-sm-3 control-label">Title</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="title2" name="title2">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">Description</label>
							<div class="col-sm-9">
								<textarea class="form-control" style="height:55px;" id="descr2" name="descr"></textarea>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default antoclose2" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary antosubmit2">Save changes</button>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/fullcalendar.min.css') }}">
<script type="text/javascript" src="{{ Package::asset('js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/fullcalendar.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var modeDelete = false;
		var clearContent = function () {
			calendar.fullCalendar('removeEvents');
		};

		var deleteContent = function (id, is_saved) {
			if (is_saved) {
				var data = {id:id};
				$.ajax({
					url: '{{ Package::route('siskol.jam.delete') }}',
					type: 'post',
					data: JSON.stringify(data),
					dataType: "json",
					contentType: "application/json",
					async: false,
					success: function (data) {
						if (data.success) {
							calendar.fullCalendar('removeEvents', id);
						} else {
							alert('error while try delete');
						}
					}
				});
			} else {
				calendar.fullCalendar('removeEvents', id);
			}
		};

		var initContent = function () {
			var tmp;

			$.ajax({
				url: '{{ Package::route('siskol.jam.ajax') }}',
				type: 'post',
				async: false,
				success: function (data) {
					calendar.fullCalendar('addEventSource', data.data);
					calendar.fullCalendar('rerenderEvents');
				}
			});
		};

		var saveAct = function () {
			var x = calendar.fullCalendar( 'clientEvents' );
			var data = [];

			$.each(x, function (index, item) {
				var mew = {};

				mew.id = ("id" in item)?item.id:null;
				mew.day = item.start.day();
				mew.start = item.start.format('HH:mm');
				mew.end = item.end.format('HH:mm');
				mew.is_break = item.is_break;
				mew.description = item.description;

				data.push(mew);
			});

			$.ajax({
				url: '{{ Package::route('siskol.jam.save') }}',
				data: JSON.stringify(data),
				dataType: "json",
				async: false,
				processData: false,
				contentType: "application/json",
				type: 'POST',
				success: function(data){
					clearContent();
					initContent();
				}
			});
		};

		var deleteAct = function (evt) {
			modeDelete = !modeDelete;

			if (modeDelete) {
				evt.target.textContent = ('Hapus Mode : ON');
			} else {
				evt.target.textContent = ('Hapus Mode : OFF');
			}
		};

		var calendar = $('#calendar').fullCalendar({
			customButtons: {
				saveButton: {
					text:'Simpan',
					click: saveAct
				},
				modeButton: {
					text: 'Mode Hapus : OFF',
					click: deleteAct
				}
			},
			header: {
				left: 'saveButton modeButton',
				center: '',
				right: ''
			},
			events: [],
			select: function(start, end, allDay) {
				var x = calendar.fullCalendar( 'clientEvents' );
				var isConflict = false;

				$.each(x, function (index, item) {
					if(start.isBetween(item.start, item.end) || end.isBetween(item.start, item.end) || item.start.isBetween(start, end) || item.end.isBetween(start, end)) {
						isConflict = true;
					}
				});

				if (start.day() != end.day()) {
					isConflict = true;
				}

				if (! isConflict) {
					calendar.fullCalendar('renderEvent', {
						start: start,
						end: end,
						allDay: false,
						is_break: false,
						description: '',
						overlap: false
					}, true);
				}

				calendar.fullCalendar( 'unselect' );
				calendar.fullCalendar( 'rerenderEvents' );
			},
			eventRender: function( event, element, view ) {
				if (event.is_break) {
					event.color = '#303030';
				} else {
					event.color = '#0FCF0F';
				}
			},
			eventClick: function(calEvent, jsEvent, view) {
				if (modeDelete) {
					var is_saved = ('id' in calEvent);

					deleteContent(calEvent._id, is_saved);
				} else {
					calEvent.is_break = !calEvent.is_break;

					calendar.fullCalendar('updateEvent', calEvent);
					calendar.fullCalendar('rerenderEvents');
					calendar.fullCalendar('unselect');
				}
			},
			columnFormat: 'dddd',
			selectable: true,
			editable: true,
			selectHelper: true,
			firstDay: 1,
			hiddenDays: [0],
			contentHeight: 1200,
			minTime: '06:00',
			maxTime: '18:00',
			slotDuration: '00:10',
			snapDuration: '00:05',
			allDaySlot:false,
			defaultView: 'agendaWeek',
			slotEventOverlap: false
		});

		initContent();
	});
</script>
@endsection
