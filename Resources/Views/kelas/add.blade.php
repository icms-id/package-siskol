@extends('base')

@section('title')
Tambah Kelas
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Tingkat <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="tingkat-select" name="tingkat" required="">
				<option></option>
				@foreach ($tingkat as $tink)
				<option value="{{ $tink['tingkat'] }}">{{ $tink['text'] }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Kelas <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<input type="text" class="form-control" name="kelas" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Kapasitas <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<input type="text" class="form-control" name="kapasitas" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Wali Kelas <span class="required">*</span></label>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="guru-select" name="guru_id" required="">
				<option></option>
				@foreach ($guru as $value)
				<option value="{{ $value->id }}">{{ $value->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('siskol.kelas.index') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>

<link href="{{ Package::asset('css/select2.min.css') }}" rel="stylesheet">
<script src="{{ Package::asset('js/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#tingkat-select').select2({ placeholder: "Pilih Tingkat", allowClear: false });
		$('#guru-select').select2({ placeholder: "Pilih Wali Kelas", allowClear: true });
	});
</script>
@endsection
