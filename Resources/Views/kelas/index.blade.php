@extends('base')

@section('title')
Daftar Kelas
@endsection

@section('package.content')
<a href="{{ Package::route('siskol.kelas.add') }}" class="btn btn-success"><i class="fa fa-pencil"></i> Tambah</a>
<table class="table table-bordered" id="data">
	<thead>
		<tr>
			<th>Kelas</th>
			<th>Jumlah Siswa</th>
			<th>Kapasitas</th>
			<th>Wali Kelas</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/dataTables.bootstrap.min.css') }}">

<script type="text/javascript" src="{{ Package::asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/buttons.print.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/responsive.bootstrap.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#data').DataTable({
			serverSide: true,
			ajax: {
				url: "{{ Package::route('siskol.kelas.ajax') }}",
				type: "POST"
			},
			columns: [
				{data: "Kelas"},
				{data: "Murid"},
				{data: "Kapasitas"},
				{
					data: "Wali_id",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<a href=\"{{ Package::route('siskol.guru.detail') }}/"+oData.Wali_id+"\">"+oData.Wali+"</a>";
						$(nTd).html(content);
					}
				},
				{
					data: "ID",
					className: "text-center",
					fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
						var content = "<a href=\"{{ Package::route('siskol.kelas.detail') }}/"+oData.ID+"\">Detail</a>";
						$(nTd).html(content);
					}
				}
			],
			responsive: true,
			ordering: false
		});
	});
</script>
@endsection
