@extends('base')

@section('title')
Detail Kelas
@endsection

@section('package.content')
<div class="col-md-3 col-sm-3 col-xs-12">
	<div class="form-horizontal form-label-left">
		<div class="form-group">
			<label>Kelas</label>
			<p class="form-control-static">VII - Static</p>
		</div>
		<div class="form-group">
			<label>Wali Kelas</label>
			<p class="form-control-static"><a href="#">Tatang</a></p>
		</div>
		<div class="form-group">
			<label>Kapasitas</label>
			<p class="form-control-static">36 <i class="fa fa-user"></i></p>
		</div>
		<div class="form-group">
			<label>Terisi</label>
			<p class="form-control-static">12 <i class="fa fa-user"></i></p>
		</div>
		<div class="ln_solid"></div>
		<div class="form-group">
			<a href="{{ Package::route('siskol.kelas.index') }}" class="btn btn-primary">Back</a>
			<button class="btn btn-success"><i class="fa fa-edit"></i> Edit</button>
			<button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
		</div>
	</div>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
	<div class="" role="tabpanel" data-example-id="togglable-tabs">
		<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
			<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Daftar Siswa</a></li>
			<li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Grafik Kehadiran</a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
			<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
				<button class="btn" id="prev-heat">Prev</button>
				<button class="btn" id="next-heat">Next</button>
				<div id="heatmap"></div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
				Grafik
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="{{ Package::asset('css/cal-heatmap.css') }}">
<script type="text/javascript" src="{{ Package::asset('js/d3.v3.min.js') }}"></script>
<script type="text/javascript" src="{{ Package::asset('js/cal-heatmap.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var cal = new CalHeatMap();
		cal.init({
			itemSelector: '#heatmap',
			domain: 'month',
			subDomain: 'day',
			cellPadding: 1,
			range: 12,
			highlight: "now",
			considerMissingDataAsZero: false,
			legend: [50, 75, 80, 95],
			start: new Date(2015,12),
			// minDate: new Date(2015, 12),
			// maxDate: new Date(2016, 11),
			domainLabelFormat: "%m-%Y",
			legendTitleFormat: {
				lower: "less than {min}%",
				inner: "between {down}% and {up}%",
				upper: "more than {max}% to 100%"
			},
			previousSelector: "#prev-heat",
			nextSelector: "#next-heat"
		});
	});
</script>
@endsection
