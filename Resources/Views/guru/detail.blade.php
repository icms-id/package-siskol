@extends('base')

@section('title')
Detail Guru
@endsection

@section('package.content')
<div class="col-md-2">
	<h2>Photo</h2>
	<img src="{{ url('images/original/8c20d976-a985-43aa-91c9-53ab88905e46.jpg') }}" class="img-responsive">
</div>
<div class="col-md-10">
	<div class="form-horizontal form-label-left" method="post">
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">NIP</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p class="form-control-static">{{ $guru->nip }}</p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p class="form-control-static">{{ $guru->name }}</p>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">Mata Pelajaran</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<p class="form-control-static">{{ $guru->pelajaran->fullname }}</p>
			</div>
		</div>
		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				<a href="{{ Package::route('siskol.guru.index') }}" class="btn btn-primary">Back</a>
				<a href="{{ Package::route('siskol.guru.edit', ['id' => $guru->id]) }}" class="btn btn-success">Edit</a>
				<a href="{{ Package::route('siskol.guru.delete', ['id' => $guru->id]) }}" class="btn btn-danger">Delete</a>
			</div>
		</div>
	</div>
</div>
@endsection
