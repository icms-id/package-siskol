@extends('base')

@section('title')
Edit Guru
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">NIP <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input class="form-control" type="text" name="nip" maxlength="20" required="" value="{{ $guru->nip }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Mata Pelajaran <span class="required">*</span></label>
		<div class="col-md-4 col-sm-4 col-xs-12">
			<select class="select2_group form-control" tabindex="-1" id="pel-select" name="pelajaran_id" required="">
				<option></option>
				@foreach ($pelajarans as $pelajaran)
				<option value="{{ $pelajaran->id }}" @if($guru->pelajaran_id == $pelajaran->id) selected @endif>{{ $pelajaran->fullname }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" name="name" maxlength="50" required="" value="{{ $guru->name }}">
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('siskol.guru.index') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>

<link href="{{ Package::asset('css/select2.min.css') }}" rel="stylesheet">
<script src="{{ Package::asset('js/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#pel-select').select2({ placeholder: "Pilih Mata Pelajaran", allowClear: false });
	});
</script>
@endsection
