@extends('base')

@section('title')
Tambah Siswa
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post">
	{!! csrf_field() !!}

	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">NISN <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<input class="form-control" type="text" name="nisn" maxlength="20" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" name="name" maxlength="50" required="">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Kelas <span class="required">*</span></label>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<select class="select2_single form-control" tabindex="-1" id="kelas-select" name="kelas_id" required="">
				<option></option>
				@foreach ($kelas as $value)
				<option value="{{ $value->id }}">{{ $value->fullname }}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('siskol.siswa.index') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>

<link href="{{ Package::asset('css/select2.min.css') }}" rel="stylesheet">
<script src="{{ Package::asset('js/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function () {
		$('#kelas-select').select2({ placeholder: "Pilih Kelas", allowClear: false });
	});
</script>
@endsection
