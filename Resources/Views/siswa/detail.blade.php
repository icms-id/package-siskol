@extends('base')

@section('title')
Detail Siswa
@endsection

@section('package.content')
<div class="col-md-3 col-sm-3 col-xs-12">
	<div class="profile_img">
		<div id="crop-avatar">
			<img class="img-responsive avatar-view" src="{{ asset('img/picture.jpg') }}" alt="Avatar" title="Change the avatar">
			<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
		</div>
	</div>

	<h3>{{ $siswa->name }}</h3>

	<ul class="list-unstyled user_data">
		<li><i class="fa fa-database user-profile-icon"></i> {{ $siswa->nisn }}</li>
		<li><i class="fa fa-institution user-profile-icon"></i> {{ $siswa->kelas->fullname }}</li>
	</ul>

	<a href="{{ Package::route('siskol.siswa.index') }}" class="btn btn-primary">Back</a>
	<a class="btn btn-success" href="{{ Package::route('siskol.siswa.edit', ['id' => $siswa->id]) }}"><i class="fa fa-edit m-right-xs"></i> Edit</a>
	<a class="btn btn-danger" href="{{ Package::route('siskol.siswa.delete', ['id' => $siswa->id]) }}"><i class="fa fa-trash m-right-xs"></i> Delete</a>
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
	<div class="" role="tabpanel" data-example-id="togglable-tabs">
		<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
			<li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Grafik Kehadiran</a></li>
			<li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Info Siswa</a></li>
		</ul>
		<div id="myTabContent" class="tab-content">
			<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
				<button class="btn" id="prev-heat">Prev</button>
				<button class="btn" id="next-heat">Next</button>
				<div id="heatmap"></div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
				Grafik
			</div>
		</div>
	</div>
</div>
@endsection
