@extends('base')

@section('title')
Edit Pelajaran
@endsection

@section('package.content')
<form class="form-horizontal form-label-left" method="post">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Kode Matpel <span class="required">*</span></label>
		<div class="col-md-2 col-sm-2 col-xs-12">
			<input class="form-control" type="text" name="kode_mat" maxlength="5" required="" value="{{$pelajaran->kode_mat}}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Mata Pelajaran <span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input class="form-control" type="text" name="pelajaran" maxlength="50" required="" value="{{ $pelajaran->pelajaran }}">
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea class="form-control" rows="2" name="keterangan" maxlength="255">{{ $pelajaran->keterangan }}</textarea>
		</div>
	</div>
	<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
			<a href="{{ Package::route('siskol.pelajaran.index') }}" class="btn btn-primary">Cancel</a>
			<button type="submit" class="btn btn-success">Submit</button>
		</div>
	</div>
</form>
@endsection
