<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;
use Package\Nothing628\Siskol\Models\ThnPelajaran;

class TahunController extends Controller {

	public function index()
	{
		return Package::view('tahun.index', [], 'Tahun Pelajaran');
	}

	public function ajax(Request $request)
	{
		$result = [];
		$tahun = ThnPelajaran::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $tahun->count();
		$result['recordsFiltered'] = $tahun->count();
		$result['data'] = [];

		$tahun = $tahun->splice($request->start ,$request->length);

		foreach ($tahun as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Tahun' => $value->fullyear,
				'Active' => $value->is_active,
			];
		}

		return response()->json($result);
	}

	public function add()
	{
		return Package::view('tahun.add', [], 'Tambah Tahun Ajar');
	}

	public function save(Request $request)
	{
		$thn = ThnPelajaran::where('thn_start', $request->thn)->get();

		if ($thn->count() > 0) {
			$thn = $thn->first();
		} else {
			$thn = new ThnPelajaran;
			$thn->thn_start = $request->thn;
			$thn->thn_end = (int)($request->thn) + 1;
		}

		$thn->is_active = $request->has('is_active');
		$thn->save();

		$this->checkActive($thn);

		return redirect()->to(Package::route('siskol.tahun.index'));
	}

	public function edit($id = null)
	{
		$tahun = ThnPelajaran::find($id);

		if ($tahun) {
			return Package::view('tahun.edit', ['tahun' => $tahun], 'Tahun Pelajaran');
		}

		return redirect()->to(Package::route('siskol.tahun.index'));
	}

	public function update(Request $request, $id)
	{
		$tahun = ThnPelajaran::find($id);

		if ($tahun) {
			$tahun->thn_start = $request->thn;
			$tahun->thn_end = (int)($request->thn) + 1;
			$tahun->is_active = $request->has('is_active');
			$tahun->save();

			$this->checkActive($tahun);
		}

		return redirect()->to(Package::route('siskol.tahun.index'));
	}

	public function delete($id = null)
	{
		$tahun = ThnPelajaran::find($id);

		if ($tahun) {
			if (! $tahun->is_active) {
				$tahun->delete();
			} else {
				return redirect()->to(Package::route('siskol.tahun.index'))->withErrors(['delete' => 'unable to delete active.']);
			}
		}

		return redirect()->to(Package::route('siskol.tahun.index'));
	}

	public function active($id = null)
	{
		$tahun = ThnPelajaran::find($id);

		if ($tahun) {
			$tahun->is_active = true;
			$tahun->save();

			$this->checkActive($tahun);
		}

		return redirect()->to(Package::route('siskol.tahun.index'));
	}

	protected function checkActive(ThnPelajaran $thn)
	{
		if ($thn->is_active) {
			ThnPelajaran::where('is_active', true)->where('id', '!=',$thn->id)->update(['is_active' => false]);
		}

		if (ThnPelajaran::where('is_active', true)->get()->count() == 0) {
			$thn->is_active = true;
			$thn->save();
		}
	}
}