<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;

class KenaikanController extends Controller {

	public function index()
	{
		return Package::view('kenaikan.index', [], 'Kenaikan');
	}
}