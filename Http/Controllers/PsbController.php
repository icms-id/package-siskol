<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;
use Package\Nothing628\Siskol\Models\ThnPelajaran as Tahun;
use Package\Nothing628\Siskol\Models\Psb;
use Package\Nothing628\Siskol\Models\PsbCalon;

class PsbController extends Controller {

	protected $pelajaran_nilai = ['Matematika', 'B Indonesia'];

	public function index()
	{
		return Package::view('psb.index', [], 'PSB Online');
	}

	public function ajax(Request $request)
	{
		$result = [];
		$pelajarans = Psb::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $pelajarans->count();
		$result['recordsFiltered'] = $pelajarans->count();
		$result['data'] = [];

		$pelajarans = $pelajarans->splice($request->start ,$request->length);

		foreach ($pelajarans as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Nama' => $value->name,
				'Tahun' => $value->tahun->fullyear,
				'Waktu' => $value->start_at->format('d M Y') . ' s/d ' . $value->end_at->format('d M Y'),
				'Status' => $value->is_active,
			];
		}

		return response()->json($result);
	}

	public function web()
	{
		$psb = Psb::listJadwal()->get();
		return Package::view('psb.web', ['psb' => $psb], 'PSB Online');
	}

	public function add()
	{
		$tahun = $this->getTahun();
		return Package::view('psb.add', ['tahun' => $tahun], 'Tambah PSB');
	}

	public function save(Request $request)
	{
		$psb = new Psb;
		$psb->thnpelajaran_id = $request->thnpelajaran_id;
		$psb->name = $request->name;
		$psb->description = $request->description;
		$psb->start_at = $request->start_at;
		$psb->end_at = $request->end_at;
		$psb->is_active = $request->has('is_active');
		$psb->save();

		return redirect()->to(Package::route('siskol.psb.index'));
	}

	public function edit($id = null)
	{
		$psb = Psb::find($id);
		$tahun = $this->getTahun();

		if ($psb) {
			return Package::view('psb.edit', ['psb' => $psb, 'tahun' => $tahun], 'Edit PSB');
		}

		return redirect()->to(Package::route('siskol.psb.index'));
	}

	public function update(Request $request, $id = null)
	{
		$psb = Psb::find($id);

		if ($psb) {
			$psb->thnpelajaran_id = $request->thnpelajaran_id;
			$psb->name = $request->name;
			$psb->description = $request->description;
			$psb->start_at = $request->start_at;
			$psb->end_at = $request->end_at;
			$psb->is_active = $request->has('is_active');
			$psb->save();
		}

		return redirect()->to(Package::route('siskol.psb.index'));
	}

	public function delete($id = null)
	{
		$psb = Psb::find($id);

		if ($psb) {
			$psb->delete();
		}

		return redirect()->to(Package::route('siskol.psb.index'));
	}

	protected function getTahun()
	{
		return Tahun::all();
	}

	public function daftar($id)
	{
		$psb = Psb::find($id);

		if ($psb) {
			return Package::view('psb.daftar', ['psb' => $psb, 'pelajaran' => $this->pelajaran_nilai], 'PSB Online');
		}

		return redirect()->to(Package::route('siskol.psb.online'));
	}

	public function prosesDaftar(Request $request, $id)
	{
		$psb = Psb::find($id);
		$calon = new PsbCalon;
		$data = $this->mappingData($request);
		$nilai = $this->mappingNilai($request);

		$calon->data = $data;
		$calon->nilai = $nilai;
		$calon->login_id = str_random(4);
		$calon->password = str_random(4);

		if ($psb) {
			$calon->psb_id = $id;
			$calon->save();

			return redirect()->to(resolveRoute('siskol.psb.finish', ['id_daftar' => $calon->id]));
		}

		return redirect()->to(resolveRoute('siskol.psb.online'));
	}

	public function finish($id_daftar)
	{
		$calon = PsbCalon::find($id_daftar);

		if ($calon) {
			return Package::view('psb.finish', ['calon' => $calon], 'PSB Online');
		}
		
		return redirect()->to(resolveRoute('siskol.psb.online'));
	}

	public function login()
	{
		return Package::view('psb.login', [], 'PSB Online');
	}

	public function postLogin(Request $request)
	{
		dd($request->all());
		return ;
	}

	protected function mappingNilai(Request $request)
	{
		$nilai = [];

		foreach ($this->pelajaran_nilai as $pelajaran)
		{
			$input = 'nilai_' . str_ireplace(' ', '_', $pelajaran);
			$nilai[$pelajaran] = $request->input($input);
		}

		return $nilai;
	}

	protected function mappingData(Request $request)
	{
		$data = [];
		$data['siswa'] = $request->only([
			'nama',
			'jenis_kelamin',
			'tmp_lahir',
			'tgl_lahir',
			'alamat',
			'no_telp',
			'sekolah_nama',
			'sekolah_alamat',
			's_kandung',
			's_tiri',
			's_angkat',
			'riwayat_penyakit',
			'tinggi',
			'berat']);
		$data['ayah']['status']			= $request->input('ayah_status');
		$data['ayah']['nama']			= $request->input('ayah_nama');
		$data['ayah']['alamat']			= $request->input('ayah_alamat');
		$data['ayah']['pendidikan']		= $request->input('ayah_pendidikan');
		$data['ayah']['agama']			= $request->input('ayah_agama');
		$data['ayah']['pekerjaan']		= $request->input('ayah_pekerjaan');
		$data['ayah']['telp']			= $request->input('ayah_telp');
		$data['ayah']['pendapatan']		= $request->input('ayah_pendapatan');
		$data['ibu']['status']			= $request->input('ibu_status');
		$data['ibu']['nama']			= $request->input('ibu_nama');
		$data['ibu']['alamat']			= $request->input('ibu_alamat');
		$data['ibu']['pendidikan']		= $request->input('ibu_pendidikan');
		$data['ibu']['agama']			= $request->input('ibu_agama');
		$data['ibu']['pekerjaan']		= $request->input('ibu_pekerjaan');
		$data['ibu']['telp']			= $request->input('ibu_telp');
		$data['ibu']['pendapatan']		= $request->input('ibu_pendapatan');
		$data['wali']['nama']			= $request->input('wali_nama');
		$data['wali']['alamat']			= $request->input('wali_alamat');
		$data['wali']['telp']			= $request->input('wali_telp');

		return $data;
	}
}
