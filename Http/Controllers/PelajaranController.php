<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package\Nothing628\Siskol\Models\Pelajaran;
use Package;

class PelajaranController extends Controller {

	public function index()
	{
		return Package::view('pelajaran.index', [], 'Daftar Pelajaran');
	}

	public function add()
	{
		return Package::view('pelajaran.add', [], 'Tambah Pelajaran');
	}

	public function save(Request $request)
	{
		$pelajaran = new Pelajaran;
		$pelajaran->kode_mat = $request->kode_mat;
		$pelajaran->pelajaran = $request->pelajaran;
		$pelajaran->keterangan = $request->keterangan;
		$pelajaran->save();

		return redirect()->to(Package::route('siskol.pelajaran.index'));
	}

	public function edit($id = null)
	{
		$pelajaran = Pelajaran::find($id);

		if ($pelajaran) {
			return Package::view('pelajaran.edit', ['pelajaran' => $pelajaran], 'Edit Pelajaran');
		}

		return redirect()->to(Package::route('siskol.pelajaran.index'));
	}

	public function update(Request $request, $id = null)
	{
		$pelajaran = Pelajaran::find($id);

		if ($pelajaran) {
			$pelajaran->kode_mat = $request->kode_mat;
			$pelajaran->pelajaran = $request->pelajaran;
			$pelajaran->keterangan = $request->keterangan;
			$pelajaran->save();

			return redirect()->to(Package::route('siskol.pelajaran.index'));
		}

		return redirect()->back()->withErrors(['pelajaran' => 'ID Pelajaran Not Found']);
	}

	public function delete($id = null)
	{
		$pelajaran = Pelajaran::find($id);

		if ($pelajaran) {
			$pelajaran->delete();
		}

		return redirect()->to(Package::route('siskol.pelajaran.index'));
	}

	public function ajax(Request $request) 
	{
		$result = [];
		$pelajarans = Pelajaran::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $pelajarans->count();
		$result['recordsFiltered'] = $pelajarans->count();
		$result['data'] = [];

		$pelajarans = $pelajarans->splice($request->start ,$request->length);

		foreach ($pelajarans as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Kode' => $value->kode_mat,
				'Pelajaran' => $value->pelajaran,
				'Keterangan' => $value->keterangan,
			];
		}

		return response()->json($result);
	}
}