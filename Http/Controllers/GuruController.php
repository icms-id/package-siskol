<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Package\Nothing628\Siskol\Models\Guru;
use Package\Nothing628\Siskol\Models\Pelajaran;
use Package;

class GuruController extends Controller {

	public function index()
	{
		return Package::view('guru.index', [], 'Daftar Guru');
	}

	public function add()
	{
		$pelajarans = $this->getPelajaran();

		return Package::view('guru.add', ['pelajarans' => $pelajarans], 'Tambah Guru');
	}

	public function ajax(Request $request)
	{
		$result = [];
		$gurus = Guru::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $gurus->count();
		$result['recordsFiltered'] = $gurus->count();
		$result['data'] = [];

		$gurus = $gurus->splice($request->start ,$request->length);

		foreach ($gurus as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'NIP' => $value->nip,
				'Pelajaran' => $value->pelajaran->fullname,
				'Nama' => $value->name,
			];
		}

		return response()->json($result);
	}

	public function save(Request $request)
	{
		$guru = new Guru;
		$guru->nip = $request->nip;
		$guru->pelajaran_id = $request->pelajaran_id;
		$guru->name = $request->name;

		try {
			$guru->save();
		} catch (QueryException $ex) {
			if ($ex->getCode() == "23505") {
				return redirect()->back()->withInput()->withErrors(['error' => 'NIP sudah terdaftar']);
			}
		}

		return redirect()->to(Package::route('siskol.guru.index'));
	}

	public function detail($id = null)
	{
		$guru = Guru::find($id);

		if ($guru) {
			return Package::view('guru.detail', ['guru' => $guru], 'Detail Guru');
		}
		
		return redirect()->to(Package::route('siskol.guru.index'));
	}

	public function edit($id = null)
	{
		$guru = Guru::find($id);
		$pelajarans = $this->getPelajaran();

		if ($guru) {
			return Package::view('guru.edit', ['guru' => $guru, 'pelajarans' => $pelajarans], 'Edit Guru');
		}

		return redirect()->to(Package::route('siskol.guru.index'));
	}

	public function update(Request $request, $id = null)
	{
		$guru = Guru::find($id);

		if ($guru) {
			$guru->nip = $request->nip;
			$guru->pelajaran_id = $request->pelajaran_id;
			$guru->name = $request->name;
			$guru->save();
		}

		return redirect()->to(Package::route('siskol.guru.index'));
	}

	public function delete($id = null)
	{
		$guru = Guru::find($id);

		if ($guru) {
			$guru->delete();
		}

		return redirect()->to(Package::route('siskol.guru.index'));
	}

	protected function getPelajaran()
	{
		$pelajarans = Pelajaran::all();

		return $pelajarans;
	}
}