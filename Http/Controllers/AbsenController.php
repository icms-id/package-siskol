<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;

class AbsenController extends Controller {

	public function index()
	{
		return Package::view('absen.index', [], 'Absensi');
	}
}