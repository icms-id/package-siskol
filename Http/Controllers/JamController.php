<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Package\Nothing628\Siskol\Models\Jam; 
use Package;

class JamController extends Controller {

	public function index()
	{
		return Package::view('jam.index', [], 'Daftar Jam Belajar');
	}

	public function save(Request $request) 
	{
		$data = $request->all();

		foreach ($data as $jam) {
			if (is_null($jam['id'])) {
				$new = new Jam;
			} else {
				$new = Jam::find($jam['id']);

				if (!$new) {
					$new = new Jam;
				}
			}
			
			$new->day_num = $jam['day'];
			$new->start_at = Carbon::parse($jam['start']);
			$new->end_at = Carbon::parse($jam['end']);
			$new->is_break = $jam['is_break'];
			$new->keterangan = $jam['description'];
			$new->save();
		}

		return response()->json(['success' => true]);
	}

	public function ajax()
	{
		$jams = Jam::all();
		$data = [];

		foreach ($jams as $jam) {
			$carbon = Carbon::now();
			$sel = $carbon->dayOfWeek - $jam->day_num;
			$at = $carbon->subDay($sel);

			$tmp['id'] = $jam->id;
			$tmp['day'] = $jam->day_num;
			$tmp['start'] = $at->toDateString() . 'T' . $jam->start_at;
			$tmp['end'] = $at->toDateString() . 'T' . $jam->end_at;
			$tmp['is_break'] = $jam->is_break;
			$tmp['description'] = $jam->keterangan;
			$tmp['overlap'] = false;

			$data['data'][] = $tmp;
		}

		return response()->json($data);
	}

	public function delete(Request $request)
	{
		$jam = Jam::find($request->id);

		if ($jam) {
			$jam->delete();

			return response()->json(['success' => true]);
		}

		return response()->json(['success' => false]);
	}
}