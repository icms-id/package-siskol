<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;
use Package\Nothing628\Siskol\Models\Kelas;
use Package\Nothing628\Siskol\Models\Guru;
use Setting;

class KelasController extends Controller {

	public function index()
	{
		return Package::view('kelas.index', [], 'Daftar Kelas');
	}

	public function add()
	{
		$tingkat = $this->getTingkat();
		$guru = $this->getGuru();
		return Package::view('kelas.add', ['tingkat' => $tingkat, 'guru' => $guru], 'Tambah Kelas');
	}

	public function save(Request $request)
	{
		$kelas = new Kelas;

		$kelas->guru_id = $request->guru_id;
		$kelas->tingkat = $request->tingkat;
		$kelas->kelas = $request->kelas;
		$kelas->kapasitas = $request->kapasitas;
		$kelas->save();

		return redirect()->to(Package::route('siskol.kelas.index'));
	}

	public function detail($id)
	{
		$kelas = Kelas::find($id);

		if ($kelas) {
			return Package::view('kelas.detail', ['kelas' => $kelas], 'Detail Kelas');
		}

		return redirect()->to(Package::route('siskol.kelas.index'));
	}

	public function ajax(Request $request)
	{
		$result = [];
		$kelases = Kelas::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $kelases->count();
		$result['recordsFiltered'] = $kelases->count();
		$result['data'] = [];

		$kelases = $kelases->splice($request->start ,$request->length);

		foreach ($kelases as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'Wali' => $value->guru->name,
				'Wali_id' => $value->guru->id,
				'Murid' => $value->siswa->count(),
				'Kelas' => $this->translateTingkat($value->tingkat) . '-' . $value->kelas,
				'Kapasitas' => $value->kapasitas,
			];
		}

		return response()->json($result);
	}

	protected function getTingkat()
	{
		$sistem = Setting::get('siskol::tingkat', null);
		if (is_null($sistem)) {
			Setting::set('siskol::tingkat', 'SMP');
		} else {
			$result = collect([]);
			switch ($sistem) {
				case 'SD':
					$result = collect([
						['tingkat' => 1, 'text' => 'I'],
						['tingkat' => 2, 'text' => 'II'],
						['tingkat' => 3, 'text' => 'III'],
						['tingkat' => 4, 'text' => 'IV'],
						['tingkat' => 5, 'text' => 'V'],
						['tingkat' => 6, 'text' => 'VI'],
					]);
					break;
				case 'SMP':
					$result = collect([
						['tingkat' => 7, 'text' => 'VII'],
						['tingkat' => 8, 'text' => 'VIII'],
						['tingkat' => 9, 'text' => 'IX'],
					]);
					break;
				case 'SMA':
					$result = collect([
						['tingkat' => 10, 'text' => 'X'],
						['tingkat' => 11, 'text' => 'XI'],
						['tingkat' => 12, 'text' => 'XII'],
					]);
					break;
			}

			return $result;
		}
	}

	protected function getGuru()
	{
		$gurus = Guru::all();

		return $gurus;
	}

	protected function translateTingkat($t)
	{
		$tingkat = [
			1 => 'I',
			2 => 'II',
			3 => 'III',
			4 => 'IV',
			5 => 'V',
			6 => 'VI',
			7 => 'VII',
			8 => 'VIII',
			9 => 'IX',
			10 => 'X',
			11 => 'XI',
			12 => 'XII',
		];

		return $tingkat[$t];
	}
}