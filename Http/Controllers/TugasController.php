<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package;

class TugasController extends Controller {

	public function index()
	{
		return Package::view('tugas.index', [], 'Tugas');
	}
}