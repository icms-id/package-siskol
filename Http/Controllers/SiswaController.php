<?php 
namespace Package\Nothing628\Siskol\Http\Controllers;

use ICMS\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Package\Nothing628\Siskol\Models\Siswa;
use Package\Nothing628\Siskol\Models\Kelas;
use Package;

class SiswaController extends Controller {

	public function index()
	{
		return Package::view('siswa.index', [], 'Daftar Siswa');
	}

	public function add()
	{
		$kelas = $this->getKelas();
		return Package::view('siswa.add', ['kelas' => $kelas], 'Tambah Siswa');
	}

	public function ajax(Request $request)
	{
		$result = [];
		$siswa = Siswa::all();

		$result['draw'] = $request->draw;
		$result['recordsTotal'] = $siswa->count();
		$result['recordsFiltered'] = $siswa->count();
		$result['data'] = [];

		$siswa = $siswa->splice($request->start ,$request->length);

		foreach ($siswa as $value) {
			$result['data'][] = [
				'ID' => $value->id,
				'NISN' => $value->nisn,
				'Name' => $value->name,
				'Kelas' => $value->kelas->fullname,
			];
		}

		return response()->json($result);
	}

	public function save(Request $request)
	{
		$siswa = new Siswa;
		$siswa->kelas_id = $request->kelas_id;
		$siswa->name = $request->name;
		$siswa->nisn = $request->nisn;
		$siswa->save();

		return redirect()->to(Package::route('siskol.siswa.index'));
	}

	public function detail($id = null)
	{
		$siswa = Siswa::find($id);

		if ($siswa) {
			return Package::view('siswa.detail', ['siswa' => $siswa], 'Detail Siswa');
		}

		return redirect()->to(Package::route('siskol.siswa.index'));
	}

	public function edit($id = null)
	{
		$siswa = Siswa::find($id);
		$kelas = $this->getKelas();

		if ($siswa) {
			return Package::view('siswa.edit', ['siswa' => $siswa, 'kelas' => $kelas], 'Edit Siswa');
		}

		return redirect()->to(Package::route('siskol.siswa.index'));
	}

	public function update(Request $request, $id = null)
	{
		$siswa = Siswa::find($id);

		if ($siswa) {
			$siswa->nisn = $request->nisn;
			$siswa->name = $request->name;
			$siswa->kelas_id = $request->kelas_id;
			$siswa->save();
		}

		return redirect()->to(Package::route('siskol.siswa.index'));
	}

	public function delete($id = null)
	{
		$siswa = Siswa::find($id);

		if ($siswa) {
			$siswa->delete();
		}

		return redirect()->to(Package::route('siskol.siswa.index'));
	}

	protected function getKelas()
	{
		return Kelas::all();
	}
}