<?php 

Route::get('/', function () {
	return "Perpus";
});

Route::get('psb', ['as' => 'siskol.psb.online', 'uses' => 'PsbController@web']);
Route::get('psb/daftar/{id}', ['as' => 'siskol.psb.daftar', 'uses' => 'PsbController@daftar']);
Route::post('psb/daftar/{id}', ['as' => 'siskol.psb.process', 'uses' => 'PsbController@prosesDaftar']);
Route::get('psb/finish/{id_daftar}', ['as' => 'siskol.psb.finish', 'uses' => 'PsbController@finish']);
Route::get('psb/login', ['as' => 'siskol.psb.login', 'uses' => 'PsbController@login']);
Route::post('psb/login', ['as' => 'siskol.psb.postlogin', 'uses' => 'PsbController@postLogin']);
