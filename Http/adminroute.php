<?php 

Route::get('/', ['as' => 'siskol.admin', 'uses' => function () {return 'siskol';}]);

Route::group(['prefix' => 'pelajaran'], function () {
	Route::get('/', ['as' => 'siskol.pelajaran.index', 'uses' => 'PelajaranController@index']);
	Route::get('add', ['as' => 'siskol.pelajaran.add', 'uses' => 'PelajaranController@add']);
	Route::post('add', ['as' => 'siskol.pelajaran.save', 'uses' => 'PelajaranController@save']);
	Route::get('edit/{id?}', ['as' => 'siskol.pelajaran.edit', 'uses' => 'PelajaranController@edit']);
	Route::post('edit/{id?}', ['as' => 'siskol.pelajaran.update', 'uses' => 'PelajaranController@update']);
	Route::get('delete/{id?}', ['as' => 'siskol.pelajaran.delete', 'uses' => 'PelajaranController@delete']);
	Route::post('ajax', ['as' => 'siskol.pelajaran.ajax', 'uses' => 'PelajaranController@ajax']);
});

Route::group(['prefix' => 'jam-belajar'], function () {
	Route::get('/', ['as' => 'siskol.jam.index', 'uses' => 'JamController@index']);
	Route::post('save', ['as' => 'siskol.jam.save', 'uses' => 'JamController@save']);
	Route::post('ajax', ['as' => 'siskol.jam.ajax', 'uses' => 'JamController@ajax']);
	Route::post('delete', ['as' => 'siskol.jam.delete', 'uses' => 'JamController@delete']);
});

Route::group(['prefix' => 'kelas'], function () {
	Route::get('/', ['as' => 'siskol.kelas.index', 'uses' => 'KelasController@index']);
	Route::get('add', ['as' => 'siskol.kelas.add', 'uses' => 'KelasController@add']);
	Route::get('detail/{id?}', ['as' => 'siskol.kelas.detail', 'uses' => 'KelasController@detail']);
	Route::post('add', ['as' => 'siskol.kelas.save', 'uses' => 'KelasController@save']);
	Route::post('ajax', ['as' => 'siskol.kelas.ajax', 'uses' => 'KelasController@ajax']);
});

Route::group(['prefix' => 'siswa'], function () {
	Route::get('/', ['as' => 'siskol.siswa.index', 'uses' => 'SiswaController@index']);
	Route::get('add', ['as' => 'siskol.siswa.add', 'uses' => 'SiswaController@add']);
	Route::post('add', ['as' => 'siskol.siswa.save', 'uses' => 'SiswaController@save']);
	Route::post('ajax', ['as' => 'siskol.siswa.ajax', 'uses' => 'SiswaController@ajax']);
	Route::get('detail/{id?}', ['as' => 'siskol.siswa.detail', 'uses' => 'SiswaController@detail']);
	Route::get('edit/{id?}', ['as' => 'siskol.siswa.edit', 'uses' => 'SiswaController@edit']);
	Route::post('edit/{id?}', ['as' => 'siskol.siswa.update', 'uses' => 'SiswaController@update']);
	Route::get('delete/{id?}', ['as' => 'siskol.siswa.delete', 'uses' => 'SiswaController@delete']);
});

Route::group(['prefix' => 'guru'], function () {
	Route::get('/', ['as' => 'siskol.guru.index', 'uses' => 'GuruController@index']);
	Route::get('add', ['as' => 'siskol.guru.add', 'uses' => 'GuruController@add']);
	Route::post('add', ['as' => 'siskol.guru.save', 'uses' => 'GuruController@save']);
	Route::post('ajax', ['as' => 'siskol.guru.ajax', 'uses' => 'GuruController@ajax']);
	Route::get('detail/{id?}', ['as' => 'siskol.guru.detail', 'uses' => 'GuruController@detail']);
	Route::get('edit/{id?}', ['as' => 'siskol.guru.edit', 'uses' => 'GuruController@edit']);
	Route::post('edit/{id?}', ['as' => 'siskol.guru.update', 'uses' => 'GuruController@update']);
	Route::get('delete/{id?}', ['as' => 'siskol.guru.delete', 'uses' => 'GuruController@delete']);
});

Route::group(['prefix' => 'thn-pljr'], function () {
	Route::get('/', ['as' => 'siskol.tahun.index', 'uses' => 'TahunController@index']);
	Route::post('ajax', ['as' => 'siskol.tahun.ajax', 'uses' => 'TahunController@ajax']);
	Route::get('add', ['as' => 'siskol.tahun.add', 'uses' => 'TahunController@add']);
	Route::post('add', ['as' => 'siskol.tahun.save', 'uses' => 'TahunController@save']);
	Route::get('edit/{id?}', ['as' => 'siskol.tahun.edit', 'uses' => 'TahunController@edit']);
	Route::post('edit/{id?}', ['as' => 'siskol.tahun.update', 'uses' => 'TahunController@update']);
	Route::get('delete/{id?}', ['as' => 'siskol.tahun.delete', 'uses' => 'TahunController@delete']);
	Route::get('active/{id?}', ['as' => 'siskol.tahun.active', 'uses' => 'TahunController@active']);
});

Route::group(['prefix' => 'tugas'], function () {
	Route::get('/', ['as' => 'siskol.tugas.index', 'uses' => 'TugasController@index']);
});

Route::group(['prefix' => 'nilai'], function () {
	Route::get('/', ['as' => 'siskol.nilai.index', 'uses' => 'NilaiController@index']);
});

Route::group(['prefix' => 'kenaikan'], function () {
	Route::get('/', ['as' => 'siskol.kenaikan.index', 'uses' => 'KenaikanController@index']);
});

Route::group(['prefix' => 'psb'], function () {
	Route::get('/', ['as' => 'siskol.psb.index', 'uses' => 'PsbController@index']);
	Route::post('ajax', ['as' => 'siskol.psb.ajax', 'uses' => 'PsbController@ajax']);
	Route::get('add', ['as' => 'siskol.psb.add', 'uses' => 'PsbController@add']);
	Route::post('add', ['as' => 'siskol.psb.save', 'uses' => 'PsbController@save']);
	Route::get('edit/{id?}', ['as' => 'siskol.psb.edit', 'uses' => 'PsbController@edit']);
	Route::post('edit/{id?}', ['as' => 'siskol.psb.update', 'uses' => 'PsbController@update']);
	Route::get('delete/{id?}', ['as' => 'siskol.psb.delete', 'uses' => 'PsbController@delete']);
	Route::get('active/{id?}', ['as' => 'siskol.psb.active', 'uses' => 'PsbController@active']);
});

Route::group(['prefix' => 'absen'], function () {
	Route::get('/', ['as' => 'siskol.absen.index', 'uses' => 'AbsenController@index']);
});
