<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model {
	use Metable;

	public $metafield = 'meta';
	public $metadata = ['sistem'];
	public $with = ['pelajaran'];

	public function pelajaran()
	{
		return $this->belongsTo(Pelajaran::class);
	}

	public function kelas()
	{
		return $this->hasOne(Kelas::class);
	}
}