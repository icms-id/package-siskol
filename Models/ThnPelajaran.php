<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThnPelajaran extends Model {
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public function scopeActive($query)
	{
		return $query->where('is_active', true);
	}

	public function getFullyearAttribute()
	{
		return $this->getAttribute('thn_start') . ' - ' . $this->getAttribute('thn_end');
	}

	public function kelas()
	{
		return $this->hasMany(Kelas::class);
	}

	public function psb()
	{
		return $this->hasMany(Psb::class);
	}

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('order', function(Builder $query) {
			return $query->orderBy('thn_start', 'desc');
		});
	}
}