<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Psb extends Model {
	protected $dates = ['start_at', 'end_at'];
	
	public function tahun()
	{
		return $this->belongsTo(ThnPelajaran::class, 'thnpelajaran_id', 'id');
	}

	public function scopeListJadwal($query)
	{
		$carbon = Carbon::now()->subDay()->format('Y-m-d H:i:s');
		return $query->where('is_active', true)->whereRaw("end_at > '" . $carbon . "'");
	}

	public function getJadwalStrAttribute()
	{
		return $this->getAttribute('start_at')->format('d M Y') . ' s/d ' . $this->getAttribute('end_at')->format('d M Y');
	}

	public function getStatusAttribute()
	{
		$carbon = Carbon::now();

		if ($carbon->gt($this->getAttribute('start_at'))) {
			return 1;
		}

		if ($carbon->gt($this->getAttribute('end_at'))) {
			return 2;
		}

		return 0;
	}
}
