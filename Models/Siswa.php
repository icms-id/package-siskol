<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model {
	use Metable;

	public $metafield = 'meta';
	public $metadata = ['sistem'];
	public $with = ['kelas'];

	public function kelas()
	{
		return $this->belongsTo(Kelas::class);
	}
}