<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Kelas extends Model {
	public $timestamps = false;
	public $table = 'kelases';

	public function guru()
	{
		return $this->belongsTo(Guru::class);
	}

	public function siswa()
	{
		return $this->hasMany(Siswa::class);
	}

	public function thnpelajaran()
	{
		return $this->belongsTo(ThnPelajaran::class);
	}

	public function getFullnameAttribute()
	{
		return $this->translateTingkat($this->tingkat) . ' - ' . $this->kelas;
	}

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope('active', function(Builder $query) {
            $tahun = ThnPelajaran::active()->get();

			if ($tahun->count() > 0) {
				$tahun = $tahun->first();
				return $query->where('thnpelajaran_id', $tahun->id);
			}

			return $query;
        });
	}

	protected function translateTingkat($t)
	{
		$tingkat = [
			1 => 'I',
			2 => 'II',
			3 => 'III',
			4 => 'IV',
			5 => 'V',
			6 => 'VI',
			7 => 'VII',
			8 => 'VIII',
			9 => 'IX',
			10 => 'X',
			11 => 'XI',
			12 => 'XII',
		];

		return $tingkat[$t];
	}
}