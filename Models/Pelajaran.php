<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;

class Pelajaran extends Model {
	public $timestamps = false;

	public function gurus()
	{
		return $this->hasMany(Guru::class);
	}

	public function getFullnameAttribute()
	{
		return $this->kode_mat . ' - ' . $this->pelajaran;
	}
}