<?php 
namespace Package\Nothing628\Siskol\Models;

use Illuminate\Database\Eloquent\Model;

class Jam extends Model {
	public $timestamps = false;
}