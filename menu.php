<?php 

return [
	'<i class="fa fa-institution"></i> Akademis' => [
		'Pelajaran'				=> 'siskol.pelajaran.index',
		'Tugas'					=> 'siskol.tugas.index',
		'Nilai'					=> 'siskol.nilai.index',
		'Kenaikan/Kelulusan'	=> 'siskol.kenaikan.index',
		'Laporan' => [
			'Nilai'				=> ''
		]
	],
	'<i class="fa fa-users"></i> Kesiswaan' => [
		'PSB'					=> 'siskol.psb.index',
		'Presensi'				=> 'siskol.absen.index',
		'Jadwal' => [
			'Jadwal'			=> '',
			'Jam Belajar'		=> 'siskol.jam.index',
		],
		'Data' => [
			'Tahun Pelajaran'	=> 'siskol.tahun.index',
			'Siswa'				=> 'siskol.siswa.index',
			'Guru'				=> 'siskol.guru.index',
			'Kelas'				=> 'siskol.kelas.index',
		],
		'Mutasi' => [
			'Siswa'				=> '',
			'Guru'				=> '',
			'Kelas'				=> '',
		],
		'Laporan' => [
			'Absen Siswa'		=> '',
			'Absen Guru'		=> '',
		]
	]
];